//console.log("Hello World");

console.log("Original Array:");
let arr = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log(arr);


function createFunction(input){
 	arr[arr.length] = input;
}
createFunction("John Cena");
console.log(arr);




function returnFunction(indexNumber){
	return arr[indexNumber];
}
let itemFound = returnFunction(2);
console.log(itemFound);




function deleteFunction() {
	return arr[arr.length-1];
}
let del = deleteFunction();
console.log(del);
arr.length--;
console.log(arr);




function updateFunction(update, index) {
	arr[index] = update;
}
updateFunction("Triple H", 3);
console.log(arr);




function deleteAllFunction(){
	arr = [];
}
deleteAllFunction();
console.log(arr);




function checkArray(){
	if(arr.length > 0){
		return false;
	}
	else
		return true;
}
let check = checkArray();
console.log(check);