//console.log("Hello");

// ARRAYS
/*
	- an array in programming is a list of data.
	- they are declared using square brackets []. also known as "Array Literals"
	- they usually used to store numberous amounts of data to manipulate in order to perform a number of tasks.
	- arrays provide a number of functions (or methods in other words) tha help in achieving this.
	- the main differenceof an array to an object is that it contains information in a form of "list", unlike object that uses properties
	- syntax:
		let/const arrayName = [elementA, elementB, elementC, etc..]
*/

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

//with Array
let studentNumber = ['2023-1928', '2023-1929', '2023-1930'];

//common examples of an array
let grades = [98.5, 94.3, 99.01, 90.1];
console.log(grades);

let computerBrands = ["Aces", "Ases", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
console.log(computerBrands);

// array with mixed data types(not recommended)
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);

//
let myTasks = ['drink HTML', "eat javascript", 'inhale CSS', "bake sass"];
console.log(myTasks);

//creating an array with values frim variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

//length property
// the .length property allows us to GET and SET the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also be used with strings. Some array methods and property can be used with strings.
let fullName = "Zybren Carbonilla";
console.log(fullName.length);

// .length property can also set the total number of items in an array. Meaning, we can actually delete the last item in the array or shorten the array by updating its length.
myTasks.length = myTasks.length -1;
console.log(myTasks.length);
console.log(myTasks);

//Using decrementation
cities.length--;
console.log(cities);
cities.length--;
console.log(cities);

//not applicable on strings
fullName.length = fullName.length -1;
console.log(fullName);
fullName.length--;
console.log(fullName);

//Adding an item in an array
//If we can shorten the array by setting the length property, we dan also lengthen it by adding a number into the length property. But it will be undefined.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

//REading from arrays
/*
	- we can access array elements through the use if array indexes
	- in JS, the first element is associated with the number 0
	- array indexes actually refer ti an address/location in the device's memory and hot the information is stored.
	- syntax:
		arrayName[index];
*/

console.log(grades[0]);
console.log(computerBrands[1]);

// accessing an array element the does not exist will return undefined
console.log(grades[20]);

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
// access the 2nd item in the array
console.log(lakersLegend[1]);

// access the 4th item in the array
console.log(lakersLegend[4]);

//You can save or store array items in another variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);


// you can re-assign aray values using the item's indices
console.log("array before assignment: ");
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("After reassignment");
console.log(lakersLegend);

//Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

//Adding Items into the array
// Using indices, we can add items into the array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[1] = "Jeru Palma";
console.log(newArr);

newArr[2] = "Mica Tingcang";
console.log(newArr);

newArr[newArr.length] = "Jenno Vea";
console.log(newArr);


// Looping over an Array
for (let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 == 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

//Multidimentional Arrays
/*
	- multidimentional arrays are usefull for storing complex data structure
*/
let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",],
	["a9", "b9", "c9", "d9", "e9", "f9", "g9", "h9",]
	]
console.log(chessBoard);

//accessing element of a multidimentional array
console.log(chessBoard[2][5]); //f3